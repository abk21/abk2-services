/*
 * Copyright (C) 2014 Philippe Tjon - A - Hen, philippe@tjonahen.nl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nl.tjonahen.abk.backend.model;

import lombok.Getter;
import lombok.Setter;
import nl.tjonahen.abk.model.Meta;

/**
 *
 * @author Philippe Tjon - A - Hen, philippe@tjonahen.nl
 */
@Getter
@Setter
public class FinancialTransaction {

    private String id;
    private Meta meta;
    private String amount;
    private String debitCreditIndicator;
    private String date;
    private String description;
    private String accountNumber;
    private String code;
    private String mutatiesoort;
    private String contraAccountName;
    private String contraAccountNumber;


    /**
     * Update the href meta data of this financial transaction.
     * 
     * @param url -
     * @return this
     */
    public FinancialTransaction updateHref(String url) {
        this.setMeta(new Meta(url + "/" + id));
        return this;
    }


}
