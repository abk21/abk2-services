/*
 * Copyright (C) 2014 Philippe Tjon - A - Hen, philippe@tjonahen.nl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nl.tjonahen.abk.upload.boundry;

import java.util.logging.Level;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import nl.tjonahen.abk.upload.entity.CsvReader;
import nl.tjonahen.abk.upload.entity.Fintransactie;
import nl.tjonahen.abk.upload.entity.Rekening;
import org.springframework.stereotype.Component;

/**
 *
 * @author Philippe Tjon - A - Hen, philippe@tjonahen.nl
 */
@Component
@Log
@RequiredArgsConstructor
public class TransactionProcessor {

    private final EntityManager entityManager;

    private Rekening findRekening(final String rekening) {
        return entityManager.find(Rekening.class, rekening);
    }

    private Rekening bepaalRekening(final Fintransactie tr) {
        Rekening rekening = findRekening(tr.getRekening());
        if (rekening == null) {
            rekening = new Rekening();
            rekening.setRekening(tr.getRekening());
            rekening.setNaam(tr.getRekening());
        }
        return rekening;
    }

    /**
     *
     * @param trans transaction to create
     */
    @Transactional
    public void process(final Fintransactie trans) {
        if (isNotExisting(trans)) {
            final Rekening rekening = bepaalRekening(trans);
            trans.setAccountRekening(rekening);
             
            entityManager.persist(trans);
        } else {
            log.log(Level.INFO, "Skipping existing Transaction {0} {1} {2}", new Object[]{trans.getDatum(), trans.getBijaf(), trans.getBedrag()});
        }
    }

    private boolean isNotExisting(final Fintransactie trans) {
        return entityManager.createNamedQuery("Fintransactie.findByHash")
                .setParameter("hash", trans.getHash())
                .getResultList()
                .isEmpty();
    }

    public CsvReader getCsvReader() {
        return entityManager.createNamedQuery("CsvReader.findAll", CsvReader.class).getSingleResult();
    }
}
