/*
 * Copyright (C) 2013 Philippe Tjon-A-Hen philippe@tjonahen.nl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nl.tjonahen.abk.upload.boundry;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import nl.tjonahen.abk.backend.model.FinancialTransaction;
import nl.tjonahen.abk.upload.entity.CsvReader;
import nl.tjonahen.abk.upload.entity.Fintransactie;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Philippe Tjon-A-Hen philippe@tjonahen.nl
 */
@RestController
@Log
@RequiredArgsConstructor
public class UploadServlet {

    private final TransactionProcessor transactionProcessor;

    @SuppressWarnings("squid:S1166") // Log or rethrow exception, ProcessingException handling is as designed. Exception is thrown from a lambda where it is also logged.
    private boolean processPartJsParsing(InputStream inputStream, CsvReader reader) {
        try {
            new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                    .lines()
                    .skip(reader.isHeaders() ? 1 : 0)
                    .forEach(s -> processLine(reader, s));
        } catch (ProcessingException ex) {
            return false;
        }

        return true;
    }

    private void processLine(CsvReader reader, String s) {
        try {
            FinancialTransaction ft = new CsvJSScripting(reader.getScript()).parse(s);
            Fintransactie trans = new Fintransactie();
            trans.setDatum(makeDate(ft.getDate()));
            trans.setTegenrekeningnaam(ft.getContraAccountName());
            trans.setRekening(ft.getAccountNumber());
            trans.setTegenrekeningrekening(ft.getContraAccountNumber());
            trans.setCode(ft.getCode());
            trans.setBijaf("debit".equals(ft.getDebitCreditIndicator()) ? "Af" : "Bij");
            trans.setBedrag(Double.valueOf(ft.getAmount().replace(',', '.')));
            trans.setMutatiesoort(ft.getMutatiesoort());
            trans.setMededeling(ft.getDescription());
            updateHash(trans);
            transactionProcessor.process(trans);
        } catch (NumberFormatException | CsvJSScripting.CsvJSScriptingException | javax.persistence.PersistenceException ex) {
            log.log(Level.SEVERE, "{0} {1} data->{2}", new Object[]{ex, ex.getMessage(), s});
            throw new ProcessingException(ex);
        }
    }

    @SuppressWarnings("squid:S4790") // MD5 hash is only calculated to determine uniqueness of the transaction
    private static void updateHash(Fintransactie ft) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(ft.getRekening().getBytes(StandardCharsets.UTF_8));
            md.update(ft.getBedrag().toString().getBytes(StandardCharsets.UTF_8));
            md.update(ft.getCode().getBytes(StandardCharsets.UTF_8));
            md.update(ft.getTegenrekeningnaam().getBytes(StandardCharsets.UTF_8));
            md.update(ft.getTegenrekeningrekening().getBytes(StandardCharsets.UTF_8));
            final Date datum = ft.getDatum();

            final LocalDateTime ldt = LocalDateTime.ofInstant(
                    Instant.ofEpochMilli(datum.getTime()), ZoneId.systemDefault());

            md.update(ldt.format(DateTimeFormatter.ISO_DATE).getBytes(StandardCharsets.UTF_8));
            md.update(ft.getBijaf().getBytes(StandardCharsets.UTF_8));
            md.update(ft.getMededeling().getBytes(StandardCharsets.UTF_8));
            md.update(ft.getMutatiesoort().getBytes(StandardCharsets.UTF_8));

            byte[] digest = md.digest();
            BigInteger bigInt = new BigInteger(1, digest);
            ft.setHash(bigInt.toString(16));
        } catch (NoSuchAlgorithmException ex) {
            log.log(Level.SEVERE, "{0} {1}", new Object[]{ex, ex.getMessage()});
            throw new ProcessingException(ex);
        }
    }

    @PostMapping(path = "/upload")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
            RedirectAttributes redirectAttributes) throws IOException {

        final CsvReader reader = transactionProcessor.getCsvReader();
        if (processPartJsParsing(file.getInputStream(), reader)) {
            redirectAttributes.addFlashAttribute("message",
                    "... process sucess... " + file.getOriginalFilename());
        } else {
            redirectAttributes.addFlashAttribute("message",
                    "... process error... " + file.getOriginalFilename());
        }

        return "redirect:/";
    }

    @GetMapping(path="/upload")
    public String doGet() {
        return "upload";
    }

    private static Date makeDate(String incDate) {
        // 20070720 yyyymmdd
        Calendar cal = new GregorianCalendar();
//CHECKSTYLE:OFF
        String strYear = incDate.substring(0, 4);
        int year = Integer.parseInt(strYear);

        String strMonth = incDate.substring(4, 6);
        int month = Integer.parseInt(strMonth) - 1;

        String strDay = incDate.substring(6, 8);
        int day = Integer.parseInt(strDay);
//CHECKSTYLE:ON
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DATE, day);

        return cal.getTime();
    }

    private static class ProcessingException extends RuntimeException {

        public ProcessingException(Throwable ex) {
            super(ex);
        }
    }

}
