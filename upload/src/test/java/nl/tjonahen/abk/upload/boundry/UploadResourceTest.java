/*
 * Copyright (C) 2016 Philippe Tjon - A - Hen, philippe@tjonahen.nl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nl.tjonahen.abk.upload.boundry;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.servlet.ServletException;
import nl.tjonahen.abk.upload.entity.CsvReader;
import nl.tjonahen.abk.upload.entity.Fintransactie;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Philippe Tjon - A - Hen, philippe@tjonahen.nl
 */
public class UploadResourceTest {

    @Mock
    private TransactionProcessor transactionProcessor;

    @Mock
    private MultipartFile multipartFile;

    @Mock
    private RedirectAttributes redirectAttributes;
            
    @InjectMocks
    private UploadServlet systemUnderTest;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testDoPost() throws ServletException, IOException {
        final CsvReader csvReader = new CsvReader();
        csvReader.setScript(loadScript());
        when(transactionProcessor.getCsvReader()).thenReturn(csvReader);
        when(multipartFile.getInputStream()).thenReturn(new ByteArrayInputStream("\"20111222\",\"1350002 NS-TIEL 201>\\TIEL> \\N\",\"5521208\",\"425008215\",\"BA\",\"Af\",\"13,00\",\"Betaalautomaat\",\"PASVOLGNR 017     22-12-2011 06:03TRANSACTIENR 1100332       \"".getBytes()));

        String redirect = systemUnderTest.handleFileUpload(multipartFile, redirectAttributes);
        
        assertEquals("redirect:/", redirect);
        verify(transactionProcessor).process(any(Fintransactie.class));
    }

    private String loadScript() {
        return getStringFromInputStream(this.getClass().getResourceAsStream("/uploadresource.js"));

    }

    // convert InputStream to String

    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }
}
