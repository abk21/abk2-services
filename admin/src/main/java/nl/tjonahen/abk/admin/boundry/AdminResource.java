/*
 * Copyright (C) 2015 Philippe Tjon - A - Hen, philippe@tjonahen.nl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nl.tjonahen.abk.admin.boundry;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import nl.tjonahen.abk.admin.entity.CsvReader;
import nl.tjonahen.abk.admin.model.CsvJsReader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Philippe Tjon - A - Hen, philippe@tjonahen.nl
 */
@Api(value = "Admin resources")
@RestController
@RequiredArgsConstructor
public class AdminResource {
    private final AdminRepository adminRepository;

    /**
     * get the current cvs reader script
     * @return -
     */
    @ApiOperation(value="Get current configured JavaScript CSV reader", response = CsvJsReader.class)
    @GetMapping(path = "/admin/csvreader", produces = MediaType.APPLICATION_JSON_VALUE)
    public CsvJsReader getCsvReader() {
        return adminRepository.findAll()
                .stream()
                .limit(1)
                .map(r -> new CsvJsReader(r.isHeaders(), r.isDryRun(), r.getScript()))
                .findFirst().orElse(null);
    }

}

interface AdminRepository extends JpaRepository<CsvReader, Long> {}
