package nl.tjonahen.abk.costcenters.boundry;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Philippe Tjon - A - Hen, philippe@tjonahen.nl
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CostCentersResourceIT {

    @LocalServerPort
    int port;

    @Before
    public void setUp() {
        RestAssured.port = port;
    }
    
    @Test
    public void testFlow() {
        given().basePath("/costcenters")
                .header(HttpHeaders.ORIGIN, "http://dummy")
                .get()
                .then()
                .statusCode(200)
                .and()
                .header("Access-Control-Allow-Origin", "*");
        
        given().basePath("/costcenters")
                .header(HttpHeaders.ORIGIN, "http://dummy")
                .body(DATA)
                .contentType(ContentType.JSON)
                .put()
                .then()
                .statusCode(202);

    }
    
    
    private static final String DATA = "[\n" +
"  {\n" +
"    \"id\": 23344,\n" +
"    \"meta\": {\n" +
"      \"href\": \"http://localhost:3002/costcenters/23344\"\n" +
"    },\n" +
"    \"filter\": \".*Q?PARK.*|.*RIJNKADEGARAGE.*|.*paardenveld.*|.*p - nieuwegein.*|.*Parkeren 16052.*|OOA.*Automaat.*|.*Nijmegen CWZ.*|.*LUCHT.*SCHIPHO.*|.*HOOG CATH*.UTRE.*|SO.*Parkeren.*UTR.*|.*BP LINGE.*|.*Parking Stad.*\",\n" +
"    \"name\": \"Auto\",\n" +
"    \"parent\": null,\n" +
"    \"list\": [\n" +
"      {\n" +
"        \"id\": 23345,\n" +
"        \"meta\": {\n" +
"          \"href\": \"http://localhost:3002/costcenters/23345\"\n" +
"        },\n" +
"        \"filter\": \".*ANWB.*\",\n" +
"        \"name\": \"ANWB\",\n" +
"        \"parent\": {\n" +
"          \"id\": 23344,\n" +
"          \"meta\": {\n" +
"            \"href\": \"http://localhost:3002/costcenters/23344\"\n" +
"          },\n" +
"          \"filter\": null,\n" +
"          \"name\": null,\n" +
"          \"parent\": null,\n" +
"          \"list\": null\n" +
"        },\n" +
"        \"list\": []\n" +
"      },\n" +
"      {\n" +
"        \"id\": 23346,\n" +
"        \"meta\": {\n" +
"          \"href\": \"http://localhost:3002/costcenters/23346\"\n" +
"        },\n" +
"        \"filter\": \".*cjib.*|.*cijb.*\",\n" +
"        \"name\": \"Centraal Justitieel Incasso Bureau (bonnen)\",\n" +
"        \"parent\": {\n" +
"          \"id\": 23344,\n" +
"          \"meta\": {\n" +
"            \"href\": \"http://localhost:3002/costcenters/23344\"\n" +
"          },\n" +
"          \"filter\": null,\n" +
"          \"name\": null,\n" +
"          \"parent\": null,\n" +
"          \"list\": null\n" +
"        },\n" +
"        \"list\": []\n" +
"      }\n" +
"    ]\n" +
"  }]";
}
