/*
 * Copyright (C) 2014 Philippe Tjon - A - Hen, philippe@tjonahen.nl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nl.tjonahen.abk.costcenters.boundry;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import nl.tjonahen.abk.costcenters.model.CostCenter;
import nl.tjonahen.abk.costcenters.model.CostCenters;
import nl.tjonahen.abk.model.HRef;
import nl.tjonahen.abk.costcenters.entity.Kostenplaats;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Philippe Tjon - A - Hen, philippe@tjonahen.nl
 */
@Api(value = "Cost centers resource.")
@RestController
@RequiredArgsConstructor
public class CostCentersResource {

    private final EntityManager entityManager;

    @ApiOperation(value = "Get list of all cost centers", response = CostCenters.class)
    @GetMapping(path = "/costcenters", produces = MediaType.APPLICATION_JSON_VALUE)
    public CostCenters get(final HttpServletRequest hsr,
            @ApiParam(value = "Level (integer) to expand the result", name = "expand")
            @RequestParam(name = "expand", defaultValue = "0") int expand) {
        return this.get(expand).updateHref(hsr.getRequestURL().toString());
    }

    @ApiOperation(value = "Get a cost center by id", response = CostCenter.class)
    @ApiResponses(
            @ApiResponse(code = 404, message = "cost center not found")
    )
    @GetMapping(path = "/costcenters/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CostCenter> get(final HttpServletRequest hsr,
            @ApiParam(value = "id of the cost center", name = "id")
            @PathVariable("id")
            final Long id,
            @ApiParam(value = "Level (integer) to expand the result", name = "expand")
            @RequestParam(name = "expand", defaultValue = "0") int expand) {
        Optional<CostCenter> optional = this.get(id, expand);
        if (optional.isPresent()) {
            return ResponseEntity.ok(optional.get().updateHref(new HRef(hsr.getRequestURL().toString()).stripId()));
        }
        return ResponseEntity.notFound().build();
    }

    /**
     * Add new cost center to the collection of cost centers
     *
     * @param hsr -
     * @param costCenter -
     * @return -
     */
    @ApiOperation(value = "Add new cost center to the collection of cost centers", response = CostCenter.class)
    @PostMapping(path = "/costcenters", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public ResponseEntity<CostCenter> addNewCostCenter(final HttpServletRequest hsr, @RequestBody CostCenter costCenter) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(this.create(costCenter).updateHref(hsr.getRequestURL().toString()));
    }

    @ApiOperation(value = "Replace the collection of cost centers")
    @PutMapping(path = "/costcenters", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public ResponseEntity replaceAllCostCenters(@RequestBody final List<CostCenter> costcenters) {
        nullIds(costcenters);
        removeAll();
        insertAll(costcenters);
        return ResponseEntity.accepted().build();
    }

    private void nullIds(List<CostCenter> costcenters) {
        costcenters.stream().map(cc -> {
            cc.setId(null);
            return cc;
        }).filter(cc -> cc.getList() != null)
                .forEach(cc -> nullIds(cc.getList()));
    }

    @ApiOperation(value = "replace cost center defined by id")
    @ApiResponses(
            @ApiResponse(code = 304, message = "In case the cost center was not found")
    )
    @PutMapping(path = "/costcenters/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity replaceACostCenter(
            @ApiParam(value = "id of the cost center", name = "id")
            @PathVariable("id")
            final Long id, @RequestBody final CostCenter costCenter) {

        if (this.update(id, costCenter)) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
    }

    @ApiOperation(value = "Delete cost center by id")
    @ApiResponses(
            @ApiResponse(code = 304, message = "In case the cost center was not found")
    )
    @DeleteMapping("/costcenters/{id}")
    public ResponseEntity delete(
            @ApiParam(value = "id of the cost center", name = "id")
            @PathVariable("id") final Long id) {
        if (this.remove(id)) {
            return ResponseEntity.accepted().build();
        }
        return ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
    }

    private CostCenters get(int expand) {
        return new CostCenters(entityManager.createNamedQuery("Kostenplaats.findByRoot", Kostenplaats.class)
                .getResultList()
                .stream()
                .map(new ConvertCostCenter(expand)::convert)
                .collect(Collectors.toList()));
    }

    private Optional<CostCenter> get(Long id, int expand) {
        return entityManager.createNamedQuery("Kostenplaats.findById", Kostenplaats.class)
                .setParameter("id", id)
                .getResultList()
                .stream()
                .map(new ConvertCostCenter(expand)::convert)
                .findFirst();
    }

    private CostCenter create(CostCenter costCenter) {
        final Kostenplaats newKostenplaats = newKostenplaats(costCenter);
        final EntityTransaction et = entityManager.getTransaction();
        et.begin();
        entityManager.persist(newKostenplaats);
        entityManager.flush();
        entityManager.refresh(newKostenplaats);
        et.commit();
        return new ConvertCostCenter(1).convert(newKostenplaats);
    }

    private static Kostenplaats newKostenplaats(CostCenter costCenter) {
        final Kostenplaats kostenplaats = new Kostenplaats();
        kostenplaats.setFilter(costCenter.getFilter());
        kostenplaats.setNaam(costCenter.getName());
        return kostenplaats;
    }

    private boolean update(Long id, CostCenter costCenter) {
        final Kostenplaats current = entityManager.find(Kostenplaats.class, id);
        if (null == current) {
            return false;
        }

        if (costCenter.getParent() != null) {
            Kostenplaats parentCurrent = entityManager.find(Kostenplaats.class, costCenter.getParent().getId());

            current.setParent(parentCurrent);
        }
        current.setFilter(costCenter.getFilter());
        current.setNaam(costCenter.getName());
        final EntityTransaction et = entityManager.getTransaction();
        et.begin();
        entityManager.merge(current);
        entityManager.flush();
        et.commit();
        return true;
    }

    private boolean remove(Long id) {
        Kostenplaats current = entityManager.find(Kostenplaats.class, id);
        if (null == current) {
            return false;
        }
        final EntityTransaction et = entityManager.getTransaction();

        et.begin();
        entityManager.remove(current);
        et.commit();
        return true;
    }

    private void removeAll() {
        entityManager.createNamedQuery("Kostenplaats.deleteAll").executeUpdate();
    }

    private void insertAll(List<CostCenter> costcenters) {
        costcenters.stream().forEach(this::insertNewRoot);
    }

    @SuppressWarnings("squid:UnusedPrivateMethod") // Sonar false positive as the method is used as a lambda
    private void insertNewRoot(CostCenter costCenter) {
        Kostenplaats current = new Kostenplaats();
        current.setFilter(costCenter.getFilter());
        current.setNaam(costCenter.getName());
        current.setKostenplaatsCollection(new ArrayList<>());
        entityManager.persist(current);
        entityManager.flush();

        if (costCenter.getList() != null) {
            costCenter.getList().stream().forEach(cc -> insertNewSub(current, cc));
        }
    }

    private void insertNewSub(Kostenplaats parent, CostCenter costCenter) {
        Kostenplaats current = new Kostenplaats();
        current.setFilter(costCenter.getFilter());
        current.setNaam(costCenter.getName());
        current.setParent(parent);
        current.setKostenplaatsCollection(new ArrayList<>());
        parent.getKostenplaatsCollection().add(current);
        entityManager.persist(current);
        entityManager.flush();

        if (costCenter.getList() != null) {
            costCenter.getList().stream().forEach(cc -> insertNewSub(current, cc));
        }

    }
}
