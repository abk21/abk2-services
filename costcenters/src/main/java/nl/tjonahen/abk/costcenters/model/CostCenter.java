/*
 * Copyright (C) 2014 Philippe Tjon - A - Hen, philippe@tjonahen.nl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nl.tjonahen.abk.costcenters.model;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import nl.tjonahen.abk.model.Meta;

/**
 *
 * @author Philippe Tjon - A - Hen, philippe@tjonahen.nl
 */
@Getter
@Setter
public class CostCenter {
    private Long id;
    private Meta meta;
    private String filter;
    private String name;
    private CostCenter parent;
    private List<CostCenter> list;
    
   
    public CostCenter updateHref(String baseUrl) {
        this.setMeta(new Meta(baseUrl + "/" + id));
        
        if (this.parent != null) {
            this.parent.updateHref(baseUrl);
        }
        
        if (this.list != null) {
            this.list.forEach(c -> c.updateHref(baseUrl));
        }
        return this;
    }
    
}
