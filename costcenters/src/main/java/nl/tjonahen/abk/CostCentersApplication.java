package nl.tjonahen.abk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class CostCentersApplication {

    public static void main(String[] args) {
        SpringApplication.run(CostCentersApplication.class);
    }

}
