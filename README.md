ABK
===

Analytisch Boekhouden en Kostencalculatie - services


## Build

### services

All the services are build using maven, run the following from top level directory to build and test

```bash
mvn clean verify
```

To build the docker images run the same command bud now include the docker profile

```bash
mvn clean verify -Pdocker
```


### front

To build the front end goto the clientjs folder and execute

```bash
npm install
bower install
grunt
```

This wil create an dist folder. for local development or testing just start a http server inside the dist folder.

To build the docker image run the following command

```bash
docker build -t tjonahen/abk2-client .
```