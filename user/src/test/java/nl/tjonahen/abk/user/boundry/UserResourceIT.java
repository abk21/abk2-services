/*
 * Copyright (C) 2017 Philippe Tjon - A - Hen philippe@tjonahen.nl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nl.tjonahen.abk.user.boundry;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Philippe Tjon - A - Hen philippe@tjonahen.nl
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserResourceIT {


    @LocalServerPort
    int port;

    @Before
    public void setUp() {
        RestAssured.port = port;
    }

    /**
     * Test of login method, of class UserResource.
     */
    @Test
    public void testFlow() throws Exception {
        given().basePath("/user/login")
                .body("{\"username\":\"testuser\", \"password\":\"password\"}")
                .contentType(ContentType.JSON)
                .post()
                .then()
                .statusCode(401);

        given().basePath("/user/create")
                .body("{\"username\":\"testuser\", \"password\":\"password\"}")
                .contentType(ContentType.JSON)
                .post()
                .then()
                .statusCode(200);

        given().basePath("/user/login")
                .body("{\"username\":\"testuser\", \"password\":\"password\"}")
                .contentType(ContentType.JSON)
                .post()
                .then()
                .statusCode(200);
    }

    @Test
    public void testNotAthorized() throws Exception {

        given().basePath("/user/")
                .contentType(ContentType.JSON)
                .get()
                .then()
                .statusCode(200);

        String token = given()
                .basePath("/user/login")
                .body("{\"username\":\"testuser\", \"password\":\"password\"}")
                .contentType(ContentType.JSON)
                .post()
                .then()
                .statusCode(200)
                .extract().header("Authorization");

        given()
                .basePath("/user/")
                .contentType(ContentType.JSON)
                .header("Authorization", token)
                .get()
                .then()
                .statusCode(200);

    }
}
