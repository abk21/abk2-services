/*
 * Copyright (C) 2017 Philippe Tjon - A - Hen philippe@tjonahen.nl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nl.tjonahen.abk.user.boundry;

import io.swagger.annotations.Api;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import nl.tjonahen.abk.user.entity.Gebruiker;
import nl.tjonahen.abk.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "User resources")
@RestController
@RequiredArgsConstructor
public class UserResource {

    private final GebruikerRepository gebruikerRepository;

    @GetMapping(path = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> getAllUsers() {
        return gebruikerRepository.findAll()
                .stream()
                .map(Gebruiker::getNaam)
                .collect(Collectors.toList());

    }

    @GetMapping(value = "/user/check", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity check() {
        return ResponseEntity.ok().build();
    }

    @PostMapping(path = "/user/login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity login(@RequestBody final User user) {
        final List<Gebruiker> gebruikers = gebruikerRepository.findByNaamAndWachtwoord(user.getUsername(), user.getPassword());
        if (gebruikers.isEmpty()) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return ResponseEntity.ok().header("Authorization", "Bearer ").build();
    }

    @PostMapping(path = "/user/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public ResponseEntity create(@RequestBody User user) {
        Gebruiker gebruiker = new Gebruiker();
        gebruiker.setNaam(user.getUsername());
        gebruiker.setWachtwoord(user.getPassword());

        gebruikerRepository.save(gebruiker);

        return ResponseEntity.ok().header("Authorization", "Bearer ").build();
    }

}

interface GebruikerRepository extends JpaRepository<Gebruiker, String> {

    List<Gebruiker> findByNaamAndWachtwoord(String naam, String wachtwoord);
}
