/*
 * Copyright (C) 2014 Philippe Tjon - A - Hen, philippe@tjonahen.nl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nl.tjonahen.abk.accounts.boundry;



import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import nl.tjonahen.abk.accounts.entity.Rekening;
import nl.tjonahen.abk.accounts.model.Account;
import nl.tjonahen.abk.accounts.model.Accounts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Philippe Tjon - A - Hen, philippe@tjonahen.nl
 */
@Api(value = "Account resources")
@RestController
@RequiredArgsConstructor
public class AccountsResource {

    private final AccountsRepository accountsRepository;

    @ApiOperation(value="Get all accounts", response = Accounts.class)
    @GetMapping(path = "/accounts", produces=MediaType.APPLICATION_JSON_VALUE)
    public Accounts get() {
        return new Accounts(accountsRepository.findAll()
                .stream()
                .map(AccountsResource::convert)
                .collect(Collectors.toList()));
    }

    private static Account convert(final Rekening rekening) {
        final Account account = new Account();
        account.setNumber(rekening.getNummer());
        account.setDescription(rekening.getNaam());
        if (rekening.getBeginsaldo() != null) {
            account.setStartsaldi(rekening.getBeginsaldo().toString());
        }
        return account;
    }
}

interface AccountsRepository extends JpaRepository<Rekening, String> {}