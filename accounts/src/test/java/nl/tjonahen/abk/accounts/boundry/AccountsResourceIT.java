package nl.tjonahen.abk.accounts.boundry;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Philippe Tjon - A - Hen, philippe@tjonahen.nl
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AccountsResourceIT {

    @LocalServerPort
    int port;

    @Before
    public void setUp() {
        RestAssured.port = port;
    }

    @Test
    public void testGet() {
        given().basePath("/accounts")
                .header(HttpHeaders.ORIGIN, "http://localhost")
                .get()
                .then()
                .statusCode(200)
                .and()
                .header("Access-Control-Allow-Origin", "*");
    }
}
