/*
 * Copyright (C) 2014 Philippe Tjon-A-Hen philippe@tjonahen.nl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name abkClientApp.controller:TransactionController
   * @description
   * # TransactionController
   * Controller of the abkClientApp
   */
  angular.module('abkClientApp').controller("TransactionController", transactionController);

  var updateCostcenterFilter = function (e) {
    if (e.filter) {
      e.filter = new RegExp(e.filter.trim(), 'i');
    }
    if (e.list) {
      e.list.forEach(updateCostcenterFilter);
    }
  };

  var matchCostCenter = function (t, c) {
    if (t.matched) {
      return;
    }
    t.matched = true;
    if (t.costcenter !== undefined)
      t.costcenter = t.costcenter + ' / ' + c.name;
    else
      t.costcenter = c.name;

  };

  var matchCostcenter = function (c, t) {
    return (c.name === t.costCenterName)
      || (c.filter
        && (c.filter.test(t.description)
          || c.filter.test(t.contraAccountName)
          || c.filter.test(t.contraAccountNumber)));
  }

  var matchSubCostCenters = function (t, sc) {
    if (matchCostcenter(sc, t)) {
      matchCostCenter(t, sc);
    }
  };
  var matchTransaction = function (t, c) {
    if (matchCostcenter(c, t)) {
      matchCostCenter(t, c);
    }
    if (c.list) {
      c.list.forEach(matchSubCostCenters.bind(null, t));
    }
  };

  var processTransaction = function (costcenters, t) {
    t.matched = false;
    if (t.debitCreditIndicator === 'debit') {
      t.amount = { amount: -parseFloat(t.amount) };
    } else {
      t.amount = { amount: parseFloat(t.amount) };
    }
    costcenters.list.forEach(matchTransaction.bind(null, t));
  };



  function transactionController($q, currentDate, transactionsService, costCentersService, userCheckService, ngDialog) {
    this.data = undefined;
    this.showall = true;
    this.range = currentDate.range();
    this.costcenters = undefined;
    this.selectedTransactions = undefined;
    this.total = undefined;

    var that = this;

    var processTransactionsAndCosteCenters = function (result) {
      that.selectedTransactions = [];

      that.data = result[0];
      that.costcenters = result[1];
      that.total = 0;

      that.costcenters.list.forEach(updateCostcenterFilter);

      that.data.list.forEach(processTransaction.bind(that, that.costcenters));

      that.data.list.forEach(t => {
        that.total += t.amount.amount;
      })
      that.data.list.sort((a, b) => {
        return a.date.localeCompare(b.date);
      });
    };

    var retrieveData = function () {
      userCheckService.check().$promise.then(function () {
        $q.all([transactionsService.get({
          q: 'date=[' + that.range.start.toJSON() + ' ' + that.range.end.toJSON() + ']',
          limit: 9999,
          fields: 'date,debitCreditIndicator,amount,description,contraAccountName,contraAccountNumber,costcentername,code',
          orderby: 'date desc'
        }).$promise,
        costCentersService.get({ expand: 3 }).$promise
        ]).then(processTransactionsAndCosteCenters);
      });

    };

    retrieveData();

    this.previous = function () {
      that.range.previous();
      that.data = undefined;

      retrieveData();
    };

    this.next = function () {
      that.range.next();
      that.data = undefined;

      retrieveData();
    };

    this.refresh = function () {
      that.data = undefined;
      retrieveData();
    }

    this.toggleSelected = function (id) {
      const index = that.selectedTransactions.indexOf(id);
      if (index > -1) {
        that.selectedTransactions.splice(index, 1);
      } else {
        that.selectedTransactions.push(id);
      }
    }

    this.isSelected = function (id) {
      const index = that.selectedTransactions.indexOf(id);
      return (index > -1);
    }

    this.displayTransaction = function (t) {
      ngDialog.openConfirm({
        template: "displayTransactionTemplateId",
        width: '60%',
        controller: ['$scope', function ($scope) {
          transactionsService.get({
            id: t.id,
            fields: 'id,date,debitCreditIndicator,amount,description,contraAccountName,contraAccountNumber,costcentername,mutatiesoort,code'
          }).$promise.then(result => {
            if (result.debitCreditIndicator === 'debit') {
              result.amount = { amount: -parseFloat(result.amount) };
            } else {
              result.amount = { amount: parseFloat(result.amount) };
            }

            $scope.transaction = result;
          })
          $scope.ok = function () {
            $scope.confirm();
          }
          $scope.clearCostCenter = function () {

            console.log("update transactie " + $scope.transaction.id + " met costcenter ");
            transactionsService.patch({ id: $scope.transaction.id }, { 'costCenterName': '' });
            retrieveData();
            $scope.confirm();

          }
        }]
      });
    }

    this.selectCostCenter = function () {
      ngDialog.openConfirm({
        template: "setCostCenterTemplateId",
        controller: ['$scope', function ($scope) {

          var processCostcenters = function (e) {
            $scope.costcenters.push(e.parent ? { 'label': ' - ' + e.name, 'name': e.name } : { 'label': e.name, 'name': e.name });
            if (e.list) {
              e.list.forEach(processCostcenters);
            }
          };

          costCentersService.get({ expand: 3 }, function (data) {
            $scope.costcenters = [];
            data.list.forEach(processCostcenters);
          });

          $scope.parent = { costcenter: undefined };


          $scope.ok = function () {
            if ($scope.parent.costcenter !== undefined) {
              that.selectedTransactions.forEach(id => {
                console.log("update transactie " + id + " met costcenter " + $scope.parent.costcenter.name);
                transactionsService.patch({ 'id': id }, { 'costCenterName': $scope.parent.costcenter.name });
              });
              retrieveData();
            }
            $scope.confirm();
          };
        }]
      });
    }
  }

})();
