/*
 * Copyright (C) 2014 Philippe Tjon-A-Hen philippe@tjonahen.nl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
(function () {

  'use strict';

  /**
   * @ngdoc function
   * @name abkClientApp.controller:HomeCtrl
   * @description
   * # HomeCtrl
   * Controller of the abkClientApp
   */
  angular.module('abkClientApp').controller("CostCalculationController", costCalculationController);

  var buildDataRow = function (that) {
    var data = [];
    for (var i = 0; i < that.lastDay; i++) {
      data.push({ day: i });
    }
    return data;
  };

  var transactieAmount = function (t) {
    if (t.debitCreditIndicator === 'debit') {
      return -t.amount;
    } else {
      return +t.amount;
    }
  }

  var updateAmount = function (that, data, t) {
    var transactionDate = new Date(t.date);
    if (transactionDate.getMonth() === that.range.start.getMonth()) {
      var day = transactionDate.getDate() - 1;
      if (data[day].amount === undefined) {
        data[day].amount = 0;
      }
      if (data[day].transactions === undefined) {
        data[day].transactions = [];
      }
      data[day].transactions.push(t);
      data[day].amount = data[day].amount + transactieAmount(t);
      t.match = true;
    }
  };
  var matchCostcenter = function (c, t) {
    return (c.name === t.costCenterName)
      || (c.filter
        && (c.filter.test(t.description)
          || c.filter.test(t.contraAccountName)
          || c.filter.test(t.contraAccountNumber)));
  }

  var processTransaction = function (that, t) {
    /* Update day column per costcenter */
    that.data.forEach(function (c) {
      if (!t.match) {
        if (matchCostcenter(c.costcenter, t)) {
          updateAmount(that, c.data, t);

        }
        c.costcenter.list.forEach(function (sc) {
          if (matchCostcenter(sc, t)) {
            updateAmount(that, c.data, t);
          }
        });
      }
    });
    /* update total per day */
    if (t.match) {
      updateAmount(that, that.total, t);
    } else {
      updateAmount(that, that.totalNoCostcenter, t);
    }
  };


  var processCostcenters = function (that, e) {
    if (e.filter) {
      e.filter = new RegExp(e.filter.trim(), 'i');
    }
    that.data.push({ sum: { amount: 0 }, costcenter: e, data: buildDataRow(that) });
    e.list = e.list ? e.list : [];
    e.list.forEach(processCostcenters.bind(null, that));
  };

  var sum = function (row) {
    return row.reduce(function (total, cell) {
      if (cell.amount) {
        return { amount: total.amount + cell.amount };
      }
      return total;
    }, { amount: 0 }).amount;
  };

  var buildDayHeader = function (that) {
    var weekdays = ["s", "m", "t", "w", "t", "f", "s"];
    var ddata = [];
    var today = new Date(that.range.start);
    for (var i = 0; i < that.lastDay; i++) {
      today.setDate(i + 1);
      ddata.push({ day: weekdays[today.getDay()] });
    }

    return ddata;

  };

  var processResult = function (that, result) {

    that.dayheader = buildDayHeader(that);
    for (var j = 0; j < that.lastDay; j++) {
      that.header.push('' + (j + 1));
    }

    that.total = buildDataRow(that);
    that.totalNoCostcenter = buildDataRow(that);
    that.data = [];

    var transactions = result[0];
    var costcenters = result[1];

    costcenters.list.forEach(processCostcenters.bind(null, that));

    transactions.list.forEach(processTransaction.bind(null, that));

    /* sum totals per costcenter */
    that.data.forEach(function (c) {
      c.sum = { amount: sum(c.data) };
    });
    that.totalTransaction = 0;
    transactions.list.forEach(t => {
      that.totalTransaction = that.totalTransaction + transactieAmount(t);
    })
    /* sum all day totals */
    that.totalAmount = sum(that.total);
    that.totalUnAccounted = sum(that.totalNoCostcenter);
  };

  var init = function (that) {
    that.lastDay = new Date(that.range.start.getFullYear(), that.range.start.getMonth() + 1, 0).getDate();
    that.data = undefined;
    that.dayheader = [];
    that.header = [];

    that.total = [];
    that.totalNoCostcenter = [];
    that.totalAmount = undefined;
    that.totalUnAccounted = undefined;
    that.totalUnmatched = undefined;
    that.totalTransaction = undefined;
  };

  function costCalculationController($q, currentDate, transactionsService, costCentersService, userCheckService) {
    this.range = currentDate.range();
    this.lastDay = undefined;
    this.data = undefined;
    this.dayheader = [];
    this.header = [];
    this.total = [];
    this.totalNoCostcenter = [];
    this.totalAmount = undefined;
    this.totalUnAccounted = undefined;
    this.totalUnmatched = undefined;
    this.totalTransaction = undefined;
    this.current;

    var that = this;

    var retrieveData = function () {
      userCheckService.check().$promise.then(function () {
        $q.all([transactionsService.get({
          q: 'date=[' + that.range.start.toJSON() + ' ' + that.range.end.toJSON() + ']', limit: 9999,
          fields: 'date,debitCreditIndicator,amount,description,contraAccountName,contraAccountNumber,costCenterName'
        }).$promise,
        costCentersService.get({ expand: 3 }).$promise]).then(processResult.bind(null, that));
      });
    };


    init(that);
    retrieveData();

    this.previous = function () {
      that.range.previous();
      init(that);
      retrieveData();
    };

    this.next = function () {
      that.range.next();
      init(that);
      retrieveData();
    };

    this.showRow = function (row) {
      if (row.costcenter.parent) {
        if (that.current) {
          return that.current.id === row.costcenter.parent.id;
        }
        return false;
      }
      return true;
    };

    this.selectRow = function (row) {
      if (that.current === row.costcenter) {
        that.current = undefined;
      } else {
        that.current = row.costcenter;
      }
    };

    this.weekday = function (index) {
      return that.dayheader[index].day !== 's';
    };
  }
  ;
})();
