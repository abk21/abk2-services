package nl.tjonahen.abk.logging;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Philippe Tjon - A - Hen
 */
@Component
@Log
public class LoggingHandlerInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info(() -> {
            final StringBuilder sb = new StringBuilder();

            sb.append("Request:\nurl     : ").append(request.getRequestURI())
              .append("\nQuery  : ").append(request.getQueryString())
              .append("\nMethod  : ").append(request.getMethod())
              .append("\nHeaders :\n");
            
            Collections.list(request.getHeaderNames()).forEach(h -> 
                    sb
                            .append(h)
                            .append(" : ")
                            .append(getHeaderValue(request, h)).append("\n"));
            sb.append("\nCookies :\n");
            if (request.getCookies() != null && request.getCookies().length > 0) {
                Arrays.asList(request.getCookies()).forEach(c -> sb.append(c.getName()).append(" : ").append(c.getValue()).append("\n"));
            }
            return sb.toString();
        });
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        log.info(() -> {
            final StringBuilder sb = new StringBuilder();
            sb.append("Response:\nurl     : ").append(request.getRequestURI())
                    .append("\nQuery   : ").append(request.getQueryString())
                    .append("\nMethod  : ").append(request.getMethod())
                    .append("\nStatus  : ").append(response.getStatus())
                    .append(" ")
                    .append(HttpStatus.valueOf(response.getStatus()).name())
                    .append("\nHeaders :\n");

            response.getHeaderNames().forEach(h -> sb.append(h).append(" : ").append(getHeaderValue(response, h)).append("\n"));
            return sb.toString();
        });
    }

    private String getHeaderValue(HttpServletRequest request, String headerName) {
        if (request.getHeader(headerName) != null) {
            return request.getHeader(headerName);
        }
        final Enumeration<String> headers = request.getHeaders(headerName);
        if (headers != null) {
            final StringBuilder sb = new StringBuilder();
            Collections.list(headers).forEach(h -> { log.info(h);sb.append(h).append(" ");});
            return sb.toString();
        }
        return "(null)";
     }
    private String getHeaderValue(HttpServletResponse response, String headerName) {
        if (response.getHeader(headerName) != null) {
            return response.getHeader(headerName);
        }
        final Collection<String> headers = response.getHeaders(headerName);
        if (headers != null) {
            final StringBuilder sb = new StringBuilder();
            headers.forEach(h -> { log.info(h);sb.append(h).append(" ");});
            return sb.toString();
        }
        return "(null)";
     }

}
