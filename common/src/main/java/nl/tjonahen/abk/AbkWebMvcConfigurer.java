package nl.tjonahen.abk;

import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import nl.tjonahen.abk.logging.LoggingHandlerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@RequiredArgsConstructor
@Log
public class AbkWebMvcConfigurer implements WebMvcConfigurer {

    private final LoggingHandlerInterceptor loggingHandlerInterceptor;

    @Bean
    public WebMvcConfigurer webConfigurer() {
        log.info("Create WebConfigurer...");
        return new WebMvcConfigurer() {
            @Override
            public void addInterceptors(final InterceptorRegistry registry) {
                registry.addInterceptor(loggingHandlerInterceptor);
            }

            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry
                        .addMapping("/**")
                        .allowedOrigins("*")
                        .allowedMethods(HttpMethod.GET.name(),
                                HttpMethod.POST.name(),
                                HttpMethod.PUT.name(),
                                HttpMethod.PATCH.name(),
                                HttpMethod.DELETE.name(),
                                HttpMethod.HEAD.name());
            }
        };
    }

}
